package de.cyberport.twitcher.windows;

import de.cyberport.twitcher.ErrorHandler;
import de.cyberport.twitcher.SceneBuilder;
import de.cyberport.twitcher.eventhandlers.KeyPressedEventHandler;
import de.cyberport.twitcher.exceptions.SettingsException;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractWindow {
    protected final Stage stage;
    protected final Group root;
    protected final Scene scene;
    protected int maxGames          = 24;
    protected int maxStreams        = 12;
    protected int maxGamesPerLine   = 8;
    protected int maxStreamsPerLine = 4;
    protected int offset            = 14;

    public AbstractWindow(final Stage stage, final String title) {
        this.stage = stage;

        root = new Group();
        scene = new Scene(root);

        scene.setOnKeyPressed(new KeyPressedEventHandler(this));

        stage.setTitle(title);
        stage.setScene(scene);
        stage.setResizable(false);

        try {
            final Properties prop = new Properties();

            prop.load(AbstractWindow.class.getResourceAsStream("/settings.properties"));

            maxGames = Integer.parseInt(prop.getProperty("maxGames"));
            maxStreams = Integer.parseInt(prop.getProperty("maxStreams"));
            maxGamesPerLine = Integer.parseInt(prop.getProperty("maxGamesPerLine"));
            maxStreamsPerLine = Integer.parseInt(prop.getProperty("maxStreamsPerLine"));
            offset = Integer.parseInt(prop.getProperty("offset"));
        } catch (IOException | NullPointerException ex) {
            Logger.getLogger(AbstractWindow.class.getName())
                    .log(Level.WARNING, "Could not load File \"settings.properties\", using fallback!", ex);
        } catch (final NumberFormatException ex) {
            ErrorHandler.print(new SettingsException("File \"settings.properties\" is malformed!", ex));
        }
    }

    protected abstract int getImgWidth();

    protected abstract int getImgHeight();

    protected abstract int getMax();

    protected abstract int getMaxPerLine();

    protected abstract void load();

    protected final void load(final VBox box) {
        root.getChildren().clear();
        root.getChildren().add(box);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                scene.setRoot(root);

                stage.sizeToScene();
                stage.show();
            }
        });
    }

    public final void reset() {
        final ProgressIndicator pi = new ProgressIndicator(-1.0f);

        final HBox hb = new HBox(pi);
        final VBox vb = new VBox(hb);

        hb.setAlignment(Pos.CENTER);
        vb.setAlignment(Pos.CENTER);

        scene.setRoot(vb);

        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                load();
            }
        });

        thread.start();
    }

    public final void init() {
        final VBox box = SceneBuilder.buildDummy(getImgWidth(), getImgHeight(), getMax(), getMaxPerLine(), offset);

        root.getChildren().clear();
        root.getChildren().add(box);

        scene.setRoot(root);

        stage.sizeToScene();
        stage.show();

        reset();
    }

    public final void show() {
        stage.show();
    }

    public final void hide() {
        stage.hide();
    }
}