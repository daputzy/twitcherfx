package de.cyberport.twitcher;

import de.cyberport.twitcher.gobblers.StreamGobbler;

import java.io.IOException;
import java.io.InputStream;

public class CommandLineManager {
    public CommandLineManager() {
    }

    private void execWinCommand(final String command) throws IOException, InterruptedException {
        final String[] cmd = new String[3];

        cmd[0] = "cmd.exe";
        cmd[1] = "/C";
        cmd[2] = command;

        final Process pr = Runtime.getRuntime().exec(cmd);

        manageOutput(pr.getErrorStream(), pr.getInputStream());

        pr.waitFor();
    }

    private void execLinuxCommand(final String command) throws IOException, InterruptedException {
        final Process pr = Runtime.getRuntime().exec(command);

        manageOutput(pr.getErrorStream(), pr.getInputStream());

        pr.waitFor();
    }

    private void execMacCommand(String command) throws IOException, InterruptedException {
        command = "/usr/local/bin/" + command;

        final Process pr = Runtime.getRuntime().exec(command);

        manageOutput(pr.getErrorStream(), pr.getInputStream());

        pr.waitFor();
    }

    private void manageOutput(final InputStream error, final InputStream output) {
        final StreamGobbler errorGobbler = new StreamGobbler(error, true);
        errorGobbler.start();

        final StreamGobbler outputGobbler = new StreamGobbler(output);
        outputGobbler.start();
    }

    public void exec(final String stream) throws IOException, InterruptedException {
        final String command = "streamlink " + stream + " best";

        if (System.getProperty("os.name").equals("Linux")) {
            execLinuxCommand(command);
        } else if (System.getProperty("os.name").equals("Mac OS X")) {
            execMacCommand(command);
        } else {
            execWinCommand(command);
        }
    }
}