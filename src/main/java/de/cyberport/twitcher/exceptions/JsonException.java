package de.cyberport.twitcher.exceptions;

import java.io.IOException;

public class JsonException extends IOException {
    public JsonException() {
        super();
    }

    public JsonException(final String message) {
        super(message);
    }

    public JsonException(final Throwable cause) {
        super(cause);
    }

    public JsonException(final String message, final Throwable cause) {
        super(message, cause);
    }
}