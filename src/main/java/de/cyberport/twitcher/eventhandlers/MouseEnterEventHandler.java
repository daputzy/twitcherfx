package de.cyberport.twitcher.eventhandlers;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;

public class MouseEnterEventHandler implements EventHandler<MouseEvent> {
    private final Scene scene;

    public MouseEnterEventHandler(final Scene scene) {
        this.scene = scene;
    }

    @Override
    public void handle(final MouseEvent event) {
        scene.setCursor(Cursor.HAND);
    }
}