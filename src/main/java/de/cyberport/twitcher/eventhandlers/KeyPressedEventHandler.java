package de.cyberport.twitcher.eventhandlers;

import de.cyberport.twitcher.windows.AbstractWindow;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class KeyPressedEventHandler implements EventHandler<KeyEvent> {
    private final AbstractWindow window;

    public KeyPressedEventHandler(final AbstractWindow window) {
        this.window = window;
    }

    @Override
    public void handle(final KeyEvent event) {
        if (event.getEventType() == KeyEvent.KEY_PRESSED) {
            if (event.getCode() == KeyCode.F5) {
                window.reset();
            }
        }
    }
}