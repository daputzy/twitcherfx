package de.cyberport.twitcher;

import de.cyberport.twitcher.windows.MessageWindow;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ErrorHandler {
    private static final Logger logger = Logger.getLogger(ErrorHandler.class.getName());

    private static MessageWindow mw;

    public static void init() {
        mw = new MessageWindow("Twitcher - ErrorHandler", true, "An error has occurred:");
    }

    public static void print(final Exception ex) {
        final StackTraceElement[] stackTrace = ex.getStackTrace();

        final ArrayList<String> texts = new ArrayList<>();

        texts.add("Exception: " + ex.getClass().getSimpleName());
        texts.add("Message: " + ex.getMessage());
        texts.add("Trace: " + stackTrace[0].toString());

        try {
            mw.add(texts, Color.BLACK);
        } catch (final NullPointerException e) {
            logger.log(Level.SEVERE, "ErrorHandler is not initialized, cannot print error!", e);
        }

        logger.log(Level.SEVERE, "An error occurred!", ex);
    }
}