package de.cyberport.twitcher.entities;

import org.json.JSONException;
import org.json.JSONObject;

public class TwitchGameEntity extends AbstractTwitchDataEntity {
    private int channels;

    public TwitchGameEntity(final int id, final String name, final String imageUrl, final int viewers, final int channels) {
        super(id, name, imageUrl, viewers);
        this.channels = channels;
    }

    public static TwitchGameEntity generateFromJSONObject(final JSONObject gameObj) throws JSONException {
        return new TwitchGameEntity(
                gameObj.getJSONObject("game").getInt("_id"),
                gameObj.getJSONObject("game").getString("name"),
                gameObj.getJSONObject("game").getJSONObject("box").getString("medium"),
                gameObj.getInt("viewers"),
                gameObj.getInt("channels")
        );
    }

    public int getChannels() {
        return channels;
    }

    public void setChannels(final int channels) {
        this.channels = channels;
    }

    @Override
    public String toString() {
        return super.toString() + " > TwitchGame{" + "channels=" + channels + '}';
    }
}