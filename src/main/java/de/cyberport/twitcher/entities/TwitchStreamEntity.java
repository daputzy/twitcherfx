package de.cyberport.twitcher.entities;

import org.json.JSONException;
import org.json.JSONObject;

public class TwitchStreamEntity extends AbstractTwitchDataEntity {
    private String game;
    private int    videoHeight;
    private double averageFps;
    private String displayName;

    public TwitchStreamEntity(
            final int id,
            final String name,
            final String imageUrl,
            final int viewers,
            final String game,
            final int videoHeight,
            final double averageFps,
            final String displayName
    ) {
        super(id, name, imageUrl, viewers);
        this.game = game;
        this.videoHeight = videoHeight;
        this.averageFps = averageFps;
        this.displayName = displayName;
    }

    public static TwitchStreamEntity generateFromJSONObject(final JSONObject streamObj) throws JSONException {
        return new TwitchStreamEntity(
                streamObj.getInt("_id"),
                streamObj.getJSONObject("channel").getString("name"),
                streamObj.getJSONObject("preview").getString("medium"),
                streamObj.getInt("viewers"),
                streamObj.getString("game"),
                streamObj.getInt("video_height"),
                streamObj.getDouble("average_fps"),
                streamObj.getJSONObject("channel").getString("display_name")
        );
    }

    public String getGame() {
        return game;
    }

    public void setGame(final String game) {
        this.game = game;
    }

    public int getVideoHeight() {
        return videoHeight;
    }

    public void setVideoHeight(final int videoHeight) {
        this.videoHeight = videoHeight;
    }

    public double getAverageFps() {
        return averageFps;
    }

    public void setAverageFps(final double averageFps) {
        this.averageFps = averageFps;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return super.toString() + " > TwitchStream{" + "game=" + game + ", video_height=" + videoHeight + ", average_fps=" + averageFps + ", display_name=" + displayName + '}';
    }
}