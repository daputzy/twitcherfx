package de.cyberport.twitcher.entities;

public abstract class AbstractTwitchDataEntity {
    protected int    id;
    protected String name;
    protected String imageUrl;
    protected int    viewers;

    public AbstractTwitchDataEntity(final int id, final String name, final String imageUrl, final int viewers) {
        this.id = id;
        this.name = name;
        this.imageUrl = imageUrl;
        this.viewers = viewers;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getViewers() {
        return viewers;
    }

    public void setViewers(final int viewers) {
        this.viewers = viewers;
    }

    @Override
    public String toString() {
        return "AbstractTwitchDataObject{" + "id=" + id + ", name=" + name + ", imageUrl=" + imageUrl + ", viewers=" + viewers + '}';
    }
}