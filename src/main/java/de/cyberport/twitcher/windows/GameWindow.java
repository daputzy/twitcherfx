package de.cyberport.twitcher.windows;

import de.cyberport.twitcher.ApiCaller;
import de.cyberport.twitcher.ErrorHandler;
import de.cyberport.twitcher.SceneBuilder;
import javafx.application.Platform;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URISyntaxException;

public class GameWindow extends AbstractWindow {
    private final int imgWidth  = 136;
    private final int imgHeight = 190;

    public GameWindow(final Stage stage) {
        super(stage, "Twitcher - TopGames");
    }

    @Override
    public void load() {
        try {
            load(SceneBuilder.buildGameBox(scene,
                    ApiCaller.getTopGames(maxGames),
                    imgWidth,
                    imgHeight,
                    maxGamesPerLine,
                    offset
            ));
        } catch (IOException | URISyntaxException ex) {
            ErrorHandler.print(ex);

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    hide();
                }
            });
        }
    }

    @Override
    protected int getImgWidth() {
        return imgWidth;
    }

    @Override
    protected int getImgHeight() {
        return imgHeight;
    }

    @Override
    protected int getMax() {
        return maxGames;
    }

    @Override
    protected int getMaxPerLine() {
        return maxGamesPerLine;
    }
}