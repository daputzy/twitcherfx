package de.cyberport.twitcher.windows;

import de.cyberport.twitcher.ApiCaller;
import de.cyberport.twitcher.ErrorHandler;
import de.cyberport.twitcher.SceneBuilder;
import javafx.application.Platform;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URISyntaxException;

public class StreamWindow extends AbstractWindow {
    private final int imgWidth  = 320;
    private final int imgHeight = 180;

    private final String game;

    public StreamWindow(final String game) {
        super(new Stage(), "Twitcher - TopStreams - Game: " + game);

        this.game = game;
    }

    @Override
    public void load() {
        try {
            load(SceneBuilder.buildStreamBox(
                    scene,
                    ApiCaller.getTopStreamsByGame(game, maxStreams),
                    imgWidth,
                    imgHeight,
                    maxStreamsPerLine,
                    offset
            ));
        } catch (IOException | URISyntaxException ex) {
            ErrorHandler.print(ex);

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    hide();
                }
            });
        }
    }

    @Override
    protected int getImgWidth() {
        return imgWidth;
    }

    @Override
    protected int getImgHeight() {
        return imgHeight;
    }

    @Override
    protected int getMax() {
        return maxStreams;
    }

    @Override
    protected int getMaxPerLine() {
        return maxStreamsPerLine;
    }
}