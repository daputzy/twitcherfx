package de.cyberport.twitcher.eventhandlers;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;

public class MouseExitEventHandler implements EventHandler<MouseEvent> {
    private final Scene scene;

    public MouseExitEventHandler(final Scene scene) {
        this.scene = scene;
    }

    @Override
    public void handle(final MouseEvent event) {
        scene.setCursor(Cursor.DEFAULT);
    }
}