package de.cyberport.twitcher;

import de.cyberport.twitcher.windows.AbstractWindow;
import de.cyberport.twitcher.windows.GameWindow;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainApp extends Application {
    @Override
    public void start(final Stage stage) {
        // initialize error handler
        ErrorHandler.init();

        final AbstractWindow window = new GameWindow(stage);
        window.init();
    }
}