package de.cyberport.twitcher;

import de.cyberport.twitcher.entities.TwitchGameEntity;
import de.cyberport.twitcher.entities.TwitchStreamEntity;
import de.cyberport.twitcher.eventhandlers.GameClickEventHandler;
import de.cyberport.twitcher.eventhandlers.MouseEnterEventHandler;
import de.cyberport.twitcher.eventhandlers.MouseExitEventHandler;
import de.cyberport.twitcher.eventhandlers.StreamClickEventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class SceneBuilder {
    public static VBox buildGameBox(
            final Scene scene, final ArrayList<TwitchGameEntity> games, final int imgWidth, final int imgHeight, final int maxPerLine, final int offset
    ) {
        final VBox vbox = new VBox();

        vbox.setSpacing(offset);
        vbox.setPadding(new Insets(offset, offset, offset, offset));

        int i = 0;

        for (final TwitchGameEntity game : games) {
            final ImageView imgView = new ImageView(new Image(game.getImageUrl()));

            imgView.setFitWidth(imgWidth);
            imgView.setFitHeight(imgHeight);

            final Label name = new Label(game.getName());
            name.setPrefWidth(imgWidth);

            final Label viewers = new Label(Integer.toString(game.getViewers()));
            viewers.setPrefWidth(imgWidth);

            final VBox elem = new VBox();

            elem.getChildren().add(imgView);
            elem.getChildren().add(name);
            elem.getChildren().add(viewers);

            final Double remainder = ((double) i % (double) maxPerLine);

            if (remainder == 0) {
                final HBox hbox = new HBox();

                hbox.setSpacing(offset);

                hbox.getChildren().add(elem);
                vbox.getChildren().add(hbox);
            } else {
                final HBox hbox = (HBox) vbox.getChildren().get(vbox.getChildren().size() - 1);

                hbox.getChildren().add(elem);
            }


            elem.setOnMouseClicked(new GameClickEventHandler(game.getName()));
            elem.setOnMouseEntered(new MouseEnterEventHandler(scene));
            elem.setOnMouseExited(new MouseExitEventHandler(scene));

            i++;
        }

        return vbox;
    }

    public static VBox buildStreamBox(
            final Scene scene, final ArrayList<TwitchStreamEntity> streams, final int imgWidth, final int imgHeight, final int maxPerLine, final int offset
    ) {
        final VBox vbox = new VBox();

        vbox.setSpacing(offset);
        vbox.setPadding(new Insets(offset, offset, offset, offset));

        int i = 0;

        for (final TwitchStreamEntity stream : streams) {
            final ImageView imgView = new ImageView(new Image(stream.getImageUrl()));

            imgView.setFitWidth(imgWidth);
            imgView.setFitHeight(imgHeight);

            final Label name = new Label(stream.getName());
            name.setPrefWidth(imgWidth);

            final Label viewers = new Label(Integer.toString(stream.getViewers()));
            viewers.setPrefWidth(imgWidth);

            final VBox elem = new VBox();

            elem.getChildren().add(imgView);
            elem.getChildren().add(name);
            elem.getChildren().add(viewers);

            final Double remainder = ((double) i % (double) maxPerLine);

            if (remainder == 0) {
                final HBox hbox = new HBox();

                hbox.setSpacing(offset);

                hbox.getChildren().add(elem);
                vbox.getChildren().add(hbox);
            } else {
                final HBox hbox = (HBox) vbox.getChildren().get(vbox.getChildren().size() - 1);

                hbox.getChildren().add(elem);
            }

            elem.setOnMouseClicked(new StreamClickEventHandler(stream.getName()));
            elem.setOnMouseEntered(new MouseEnterEventHandler(scene));
            elem.setOnMouseExited(new MouseExitEventHandler(scene));

            i++;
        }

        return vbox;
    }

    public static VBox buildDummy(final int width, final int height, final int max, final int maxPerLine, final int offset) {
        final VBox vbox = new VBox();

        vbox.setSpacing(offset);
        vbox.setPadding(new Insets(offset, offset, offset, offset));

        for (int i = 0; i < max; i++) {
            final Canvas canvas = new Canvas();

            canvas.setWidth(width);
            canvas.setHeight(height);

            final Label name = new Label("");
            name.setPrefWidth(width);

            final Label viewers = new Label("");
            viewers.setPrefWidth(width);

            final VBox elem = new VBox();

            elem.getChildren().add(canvas);
            elem.getChildren().add(name);
            elem.getChildren().add(viewers);

            final Double remainder = ((double) i % (double) maxPerLine);

            if (remainder == 0) {
                final HBox hbox = new HBox();

                hbox.setSpacing(offset);

                hbox.getChildren().add(elem);
                vbox.getChildren().add(hbox);
            } else {
                final HBox hbox = (HBox) vbox.getChildren().get(vbox.getChildren().size() - 1);

                hbox.getChildren().add(elem);
            }
        }

        return vbox;
    }
}