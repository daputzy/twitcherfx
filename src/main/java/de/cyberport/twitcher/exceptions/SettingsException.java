package de.cyberport.twitcher.exceptions;

public class SettingsException extends Exception {
    public SettingsException() {
        super();
    }

    public SettingsException(final String message) {
        super(message);
    }

    public SettingsException(final Throwable cause) {
        super(cause);
    }

    public SettingsException(final String message, final Throwable cause) {
        super(message, cause);
    }
}