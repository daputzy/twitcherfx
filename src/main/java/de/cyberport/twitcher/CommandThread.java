package de.cyberport.twitcher;

import java.io.IOException;

public class CommandThread implements Runnable {
    private final String stream;

    public CommandThread(final String stream) {
        this.stream = stream;
    }

    @Override
    public void run() {
        final CommandLineManager clm = new CommandLineManager();

        try {
            clm.exec(stream);
        } catch (IOException | InterruptedException ex) {
            ErrorHandler.print(ex);
        }
    }
}