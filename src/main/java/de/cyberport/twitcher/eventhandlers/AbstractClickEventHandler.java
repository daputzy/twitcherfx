package de.cyberport.twitcher.eventhandlers;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public abstract class AbstractClickEventHandler implements EventHandler<MouseEvent> {
    protected String name;

    public AbstractClickEventHandler(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}