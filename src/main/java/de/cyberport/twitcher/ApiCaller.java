package de.cyberport.twitcher;

import de.cyberport.twitcher.entities.TwitchGameEntity;
import de.cyberport.twitcher.entities.TwitchStreamEntity;
import de.cyberport.twitcher.exceptions.JsonException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class ApiCaller {
    public static ArrayList<TwitchGameEntity> getTopGames() throws IOException, URISyntaxException {
        return getTopGames(10);
    }

    public static ArrayList<TwitchGameEntity> getTopGames(final int limit) throws IOException, URISyntaxException {
        return getTopGames(limit, 0);
    }

    public static ArrayList<TwitchGameEntity> getTopGames(
            final int limit, final int offset
    ) throws IOException, URISyntaxException {
        final String responseString = call("/kraken/games/top", "limit=" + limit + "&offset=" + offset);

        final JSONObject gamesObj   = new JSONObject(responseString);
        final JSONArray  gamesArray = gamesObj.getJSONArray("top");

        final ArrayList<TwitchGameEntity> games = new ArrayList<>();

        try {
            for (int i = 0; i < gamesArray.length(); i++) {
                games.add(TwitchGameEntity.generateFromJSONObject(gamesArray.getJSONObject(i)));
            }
        } catch (NullPointerException | JSONException ex) {
            throw new JsonException("Could not parse Data!", ex);
        }

        return games;
    }

    public static ArrayList<TwitchStreamEntity> getTopStreamsByGame(final String game)
            throws IOException, URISyntaxException {
        return getTopStreamsByGame(game, 10, 0);
    }

    public static ArrayList<TwitchStreamEntity> getTopStreamsByGame(
            final String game, final int limit
    ) throws IOException, URISyntaxException {
        return getTopStreamsByGame(game, limit, 0);
    }

    public static ArrayList<TwitchStreamEntity> getTopStreamsByGame(
            final String game, final int limit, final int offset
    ) throws IOException, URISyntaxException {
        final String responseString = call("/kraken/streams", "game=" + game + "&limit=" + limit + "&offset=" + offset);

        final JSONObject streamsObj   = new JSONObject(responseString);
        final JSONArray  streamsArray = streamsObj.getJSONArray("streams");

        final ArrayList<TwitchStreamEntity> streams = new ArrayList<>();

        try {
            for (int i = 0; i < streamsArray.length(); i++) {
                streams.add(TwitchStreamEntity.generateFromJSONObject(streamsArray.getJSONObject(i)));
            }
        } catch (NullPointerException | JSONException ex) {
            throw new JsonException("Could not parse Data!", ex);
        }

        return streams;
    }

    private static String call(final String path, final String params) throws URISyntaxException, IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        final URI        uri    = new URI("https", "api.twitch.tv", path, params, null);

        final HttpGet request = new HttpGet(uri.toString());

        request.setHeader("Client-ID", "jj7ildgpuryem1hk1h2vb3ofuti4vp0");

        final BufferedReader rd = new BufferedReader(
                new InputStreamReader(client.execute(request).getEntity().getContent(),
                        "UTF-8"
                ));

        final StringBuilder sb = new StringBuilder();
        String              line;

        while ((line = rd.readLine()) != null) {
            sb.append(line).append("\r");
        }

        return sb.toString();
    }
}