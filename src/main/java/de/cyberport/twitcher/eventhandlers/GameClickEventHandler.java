package de.cyberport.twitcher.eventhandlers;

import de.cyberport.twitcher.windows.AbstractWindow;
import de.cyberport.twitcher.windows.StreamWindow;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class GameClickEventHandler extends AbstractClickEventHandler {
    public GameClickEventHandler(final String name) {
        super(name);
    }

    @Override
    public void handle(final MouseEvent t) {
        if (t.getButton().equals(MouseButton.PRIMARY)) {
            final AbstractWindow window = new StreamWindow(name);

            window.init();
        }
    }
}