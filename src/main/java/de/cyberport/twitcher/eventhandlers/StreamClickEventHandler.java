package de.cyberport.twitcher.eventhandlers;

import de.cyberport.twitcher.CommandThread;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class StreamClickEventHandler extends AbstractClickEventHandler {
    public StreamClickEventHandler(final String name) {
        super(name);
    }

    @Override
    public void handle(final MouseEvent t) {
        if (t.getButton().equals(MouseButton.PRIMARY)) {
            final CommandThread ct = new CommandThread("twitch.tv/" + name);

            final Thread thread = new Thread(ct);
            thread.start();
        }
    }
}