package de.cyberport.twitcher.listeners;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.scene.control.ScrollPane;

public class ScrollListener implements InvalidationListener {
    private final ScrollPane sp;

    public ScrollListener(final ScrollPane sp) {
        this.sp = sp;
    }

    @Override
    public void invalidated(final Observable observable) {
        sp.setVvalue(sp.getVmax());
    }
}