package de.cyberport.twitcher.windows;

import de.cyberport.twitcher.listeners.ScrollListener;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.ArrayList;

public class MessageWindow {
    private final Stage      stage  = new Stage();
    private final ScrollPane sp     = new ScrollPane();
    private final VBox       root   = new VBox();
    private final int        offset = 14;

    public MessageWindow(final String title, final boolean alwaysOnTop) {
        init(title, alwaysOnTop);
    }

    public MessageWindow(final String title, final boolean alwaysOnTop, final String heading) {
        init(title, alwaysOnTop);

        final Label label = new Label(heading);
        label.setFont(Font.font(18));

        root.getChildren().add(label);
    }

    private void init(final String title, final boolean alwaysOnTop) {
        root.setSpacing(10);
        root.setPadding(new Insets(offset, offset, offset, offset));

        root.heightProperty().addListener(new ScrollListener(sp));

        sp.setFitToHeight(true);
        sp.setFitToWidth(true);

        sp.setMaxHeight(720);
        sp.setMaxWidth(1280);

        sp.setContent(root);

        stage.setScene(new Scene(sp));
        stage.setTitle(title);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.setAlwaysOnTop(alwaysOnTop);
    }

    public void add(final String text, final Paint color) {
        final ArrayList<String> texts = new ArrayList<>();

        texts.add(text);

        add(texts, color);
    }

    public void add(final ArrayList<String> texts, final Paint color) {
        if (texts.isEmpty()) {
            return;
        }

        final VBox box = new VBox();

        box.setSpacing(2);

        for (final String text : texts) {
            final Label label = new Label(text);

            label.setTextFill(color);

            box.getChildren().add(label);
        }

        addRegion(box);
    }

    private void addRegion(final Region region) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                root.getChildren().add(region);

                stage.show();
                stage.sizeToScene();
                stage.centerOnScreen();
            }
        });
    }
}