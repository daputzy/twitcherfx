package de.cyberport.twitcher.gobblers;

import de.cyberport.twitcher.ErrorHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StreamGobbler extends Thread {
    private static final Logger logger = Logger.getLogger(StreamGobbler.class.getName());

    private final InputStream is;
    private final Boolean     isError;

    public StreamGobbler(final InputStream is, final Boolean isError) {
        this.is = is;
        this.isError = isError;
    }

    public StreamGobbler(final InputStream is) {
        this(is, false);
    }

    @Override
    public void run() {
        final BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String               line;

        try {
            while ((line = br.readLine()) != null) {
                print(line);
            }
        } catch (final IOException ex) {
            ErrorHandler.print(ex);
        }
    }

    private void print(final String line) {
        if (isError) {
            logger.log(Level.WARNING, line);
        } else {
            logger.log(Level.INFO, line);
        }
    }
}